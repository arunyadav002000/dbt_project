from datetime import datetime ,timedelta

from airflow import DAG

from airflow.operators.bash import BashOperator
from airflow.operators.email import EmailOperator

default_args={
    'owner':'Arun',
    'retries': 5,
    'retry_delay': timedelta(minutes=2)
}

with DAG(
    dag_id="email_dag",
    default_args=default_args,
    description='this is my first dag',
    start_date=datetime(2021,7,29,2),
    schedule_interval='@daily'

) as dag:
    task1=BashOperator(
        task_id='task1',
        bash_command='echo "Hello, World!"',

    )
    task2=BashOperator(
        task_id='task2',
        bash_command='echo "Hello, World!"',

    )
    task3=EmailOperator(
        task_id='task3',
        to='aruyadav077@gmail.com',
        subject='Airflow Alert',
        html_content=""" <h3>email test operator</h3>""",
    )

# Task Dependency

    task1>>task2>>task3
    

# #   set_downstream and ">>" both are working same
#     # task1 >> task2
#     # task1 >> task3










# # import datetime

# # import pendulum

# # from airflow.models.dag import DAG
# # from airflow.operators.empty import EmptyOperator

# # now = pendulum.now(tz="UTC")
# # now_to_the_hour = (now - datetime.timedelta(0, 0, 0, 0, 0, 3)).replace(minute=0, second=0, microsecond=0)
# # START_DATE = now_to_the_hour
# # DAG_NAME = "test_dag_v1"
# # dag = DAG(
# #     DAG_NAME,
# #     schedule="*/10 * * * *",
# #     default_args={"depends_on_past": True},
# #     start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
# #     catchup=False,
# # )

# # run_this_1 = EmptyOperator(task_id="run_this_1", dag=dag)
# # run_this_2 = EmptyOperator(task_id="run_this_2", dag=dag)
# # run_this_2.set_upstream(run_this_1)
# # run_this_3 = EmptyOperator(task_id="run_this_3", dag=dag)
# # run_this_3.set_upstream(run_this_2)