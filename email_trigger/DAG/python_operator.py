import boto3
import os
from datetime import datetime,timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.amazon.aws.operators.sns import SnsPublishOperator    
import yaml
from markupsafe import Markup
import sys
# from dag.dbt_project.email_trigger.DAG.info_cryo import get_snstopic

sys.path.insert(0, './dags/dbt_project/email_trigger/DAG')

import info_cryo as info

# sns_topic_arn='arn:aws:sns:us-east-1:303587856972:email_notification'



sns_topic_arn = info.get_snstopic()

# s3=boto3.client('s3',
#                  aws_access_key_id = 'AKIAUNLZ7CJGFFSNQHOT',
#                  aws_secret_access_key = '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',
#                  region_name = 'us-east-1')

# bucket_name = 'mydbtbucket01'
# key = 'DAG/dbt_project/email_trigger/config/corn_schedule.yml'

# # use the S3 client to get the object
# obj = s3.get_object(Bucket=bucket_name, Key=key)
# stream = obj['Body'].read()
# config = yaml.safe_load(stream)

config=info.read_S3("cron_schedule.yml")

def my_task():
    print(os.listdir("./dags/dbt_project/email_trigger/DAG"))
    print("welcome to spark")
    current_dir = os.getcwd()
    print(current_dir)

# def generate_html_link(url, text):
#     link_html = f'<a href="{url}">{text}</a>'
#     return Markup(link_html)
    

# def send_failure_notification(context):
#     task_instance = context.get("task_instance")
#     task_id = task_instance.task_id
#     dag_id = task_instance.dag_id
#     execution_date = task_instance.execution_date
#     aws_conn_id = "aws_default"
#     log_url = task_instance.log_url
#     custom_text = "click me"
   
   
#     link_html = generate_html_link(log_url, custom_text)

#     message = f"Task {task_id} in  DAG {dag_id} failed/success at {execution_date}. Log URL: {link_html}"
    
#     # message = message.replace(link_html, custom_text)

#     sns_operator = SnsPublishOperator(
#         task_id='sns_publish',
#         message=message,
#         aws_conn_id=aws_conn_id,
#         target_arn=sns_topic_arn,
#         dag=dag
#     )

#     sns_operator.execute(context=context)
    
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 5, 8),
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

with DAG(
    dag_id='sns_email_notification',
    default_args=default_args,
    schedule_interval=config["schedule_interval"]["cron"],
) as dag:

   
    task = PythonOperator(
        task_id='my_task',
        python_callable=my_task,
        # on_failure_callback=send_failure_notification,
        # on_success_callback=send_failure_notification,
        dag=dag
    )
 