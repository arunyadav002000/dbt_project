from airflow import DAG
from datetime import datetime, timedelta
from airflow.providers.http.sensors.http import HttpSensor
# from airflow.sensors.filesystem import FileSensor
from airflow.operators.email import EmailOperator
default_args = {
    "owner": "airflow",
    "email_on_failure": False,
    "email_on_retry": "aruyadav077@gmail.com",
    "retries": 1,
    "retry_delay": timedelta(minutes=5)
}
with DAG(
    
     dag_id="medium_test_DAG",
     start_date=datetime(2021, 1, 1),  
     schedule_interval="@daily", 
     default_args=default_args, 
     catchup=False
 )as dag:
    
    check_API_Availability = HttpSensor(
            task_id="check_API_Availability",
            http_conn_id="GIT_API",
            endpoint="arunyadav002000",
            # response_check=lambda response: "pullrequests" in response.text,
            poke_interval=5,
            timeout=20
        )
    # check_localFile1_Availability = FileSensor(
    #                 task_id="check_localFile1_Availability",
    #                 fs_conn_id="file_path",
    #                 filepath="localtestFile.csv",
    #                 poke_interval=5,
    #                 timeout=20
    #     )
    # check_localFile2_Availability = FileSensor(
    #                 task_id="check_localFile2_Availability",
    #                 fs_conn_id="file_path",
    #                 filepath="localtestFile.csv",
    #                 poke_interval=5,
    #                 timeout=20
    #     )
    send_email = EmailOperator(
                    task_id = "send_email_notication",
                    to="aruyadav077@gmail.com",
                    subject="Testing the email task",
                    html_content="<h3>Darth Vader/Yoda</h3>"
        )
    

check_API_Availability >> send_email

# >> [check_localFile1_Availability, check_localFile2_Availability] 