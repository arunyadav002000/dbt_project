import os
import boto3
import requests
from bs4 import BeautifulSoup
from botocore.exceptions import ClientError
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from datetime import datetime,timedelta
from airflow import DAG
from airflow.operators.python import PythonOperator
import sys
sys.path.insert(0, './dags/dbt_project/email_trigger/DAG')
import info_cryo as info


SENDER ="aruyadav077@gmail.com"
RECIPIENT = ["arunyadav002000@gmail.com","ktapms@gmail.com"]
SUBJECT = "DAG Notification"
# The configuration set used to track mails
# CONFIGURATION_SET = "ConfigSet"

config=info.read_S3("cron_schedule.yml")

def my_task():
    print("welcome to spark")
    current_dir = os.getcwd()
    print(current_dir)


# def find(url,list):
#     attempt_info = url.split("attempt ")[1].split(" ")[0]
#     list.append(attempt_info)
#     error_msg_start = url.find("Task exited with return code")
#     if error_msg_start != -1:
#         error_msg_end = url.find("\n", error_msg_start)
#         error_msg = url[error_msg_start:error_msg_end]
#         # print("Error Message:", error_msg)
#         list.append(error_msg)
#     else:
#         # print("No error message found.")\
#         list.append("no error message")


def send_failure_notification(context):

    task_instance = context.get("task_instance")
    task_id = task_instance.task_id
    dag_id = task_instance.dag_id
    execution_date = task_instance.execution_date
    aws_conn_id = "aws_default"
    log_url = task_instance.log_url
    # contents = task_instance.xcom_pull(key=log_url, task_ids=task_id, include_prior_dates=False)
    
    # response = requests.get(log_url)
    # html_content = response.text
    # soup = BeautifulSoup(html_content, 'html.parser')

    # # Find the element containing the log contents
    # log_element = soup.find('pre', {'class': 'log'})

    # # Extract the log contents
    # log_contents = log_element.text if log_element else ''
    # print(log_contents)
    # # log_content=[]
    # # find(contents,log_content)

    
    # The email body for recipients with non-HTML email clients.
    BODY_TEXT = "Hello,\r\nPlease see the attached file for a list of customers to contact."
    # The HTML body of the email.
    BODY_HTML = """\
    <html>
    <head></head>
    <body>
    <h1>task success!</h1>
    <p1>DAG_id:- {dag_id}</p1> <br>
    <p2>Task_id:- {task_id} </p2> <br>
    <p3> Execution_date_of_DAG_id :- {execution_date}<p3><br>
    <p4> LOG_LINK :- </p4>
    <a href ="{log_url}">click</a> 
    </body>
    </html>
    """.format(dag_id=dag_id,task_id=task_id,execution_date=execution_date,log_url=log_url,)
    # The character encoding for the email.
    CHARSET = "utf-8"
    # Create a new SES resource and specify a region.
    client = boto3.client('ses')
    # Create an instance of multipart/mixed parent container.
    msg = MIMEMultipart('mixed')
    # Add subject, from and to lines.
    msg['Subject'] = SUBJECT 
    msg['From'] = SENDER 
    # msg['To'] = RECIPIENT
    msg['To'] = ', '.join(RECIPIENT)
    # Create a multipart/alternative child container.
    msg_body = MIMEMultipart('alternative')
    # Encode the text and HTML content and set the character encoding. This step is
    # necessary if you're sending a message with characters outside the ASCII range.
    
    htmlpart = MIMEText(BODY_HTML.encode(CHARSET), 'html', CHARSET)
    # Add the text and HTML parts to the child container.
    
    msg_body.attach(htmlpart)
 
    msg.attach(msg_body)
  
    try:
        #Provide the contents of the email.
        response = client.send_raw_email(
            Source=SENDER,
            Destinations=RECIPIENT,
            RawMessage={
                'Data':msg.as_string(),
            },
            
        )
    # Display an error if something goes wrong.	
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 5, 8),
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}    


with DAG(
    dag_id='ses_notification',
    default_args=default_args,
    schedule_interval=config["schedule_interval"]["cron"],
) as dag:

   
    task = PythonOperator(
        task_id='my_task',
        python_callable=my_task,
        # on_failure_callback=send_failure_notification,
        on_success_callback=send_failure_notification,
        dag=dag
    )