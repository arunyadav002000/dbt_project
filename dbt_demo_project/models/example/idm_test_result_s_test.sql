{{ config(materialized='incremental',unique_key = ['deposit_id','test_name','test_type'], sort='deposit_id') }}

WITH ABC AS (
  SELECT t.id,
  t.asset_id,
  t.opportunity_name,
  t.deposit_id,
  t.test_name,
  t.test_type,
  t.test_date,
  t.test_result,
  t.result_notes,
  t.reason_not_tested,
  t.result_entered_by,
  t.result_entered_date,
  t.flag_voided,
  t.created_by,
  t.create_timestamp,
  t.modified_by,
  t.modified_timestamp,
  t.flag_vrl
FROM  spectrum_cur_laboratory.idm_test_result t
),

DEF AS 
(SELECT deposit_id,
test_name,
test_type,
MAX(modified_timestamp) as modified_timestamp
FROM spectrum_cur_laboratory.idm_test_result
GROUP BY deposit_id, test_name, test_type),

idm_test_result_s_test AS (
  SELECT abc.* FROM DEF INNER JOIN ABC
    ON abc.deposit_id = def.deposit_id
    AND abc.test_name =  def.test_name
    AND abc.test_type =  def.test_type
    AND abc.modified_timestamp = def.modified_timestamp) 

select * from idm_test_result_s_test

{% if is_incremental() %}

  -- this filter will only be applied on an incremental run
  where modified_timestamp > (select modified_timestamp from {{ this }})

{% endif %}


