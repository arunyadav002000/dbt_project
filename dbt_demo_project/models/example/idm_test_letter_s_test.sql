{{ config(materialized='incremental',unique_key = 'deposit_id') }}

WITH ABC AS (
  SELECT 
  t.deposit_id,
  t.mom_letter_printed,
  t.mom_letter_printed_date,
  t.doc_letter_printed,
  t.doc_letter_printed_date,
  t.state_letter_printed,
  t.state_letter_printed_date,
  t.mom_letter_sent,
  t.mom_letter_sent_date,
  t.doc_letter_sent,
  t.doc_letter_sent_date,
  t.state_letter_sent,
  t.state_letter_sent_date,
  t.mom_address_in_us,
  t.created_by,
  t.create_timestamp,
  t.modified_by,
  t.modified_timestamp
FROM spectrum_cur_laboratory.idm_test_letter t
), 


DEF AS
(SELECT deposit_id,
MAX(modified_timestamp) as modified_timestamp
FROM spectrum_cur_laboratory.idm_test_letter 
GROUP BY deposit_id) ,


idm_test_letter_s_test AS (
  SELECT abc.* FROM ABC INNER JOIN DEF
    ON ABC.deposit_id = DEF.deposit_id
    AND ABC.modified_timestamp = DEF.modified_timestamp) 
	
	
select * from idm_test_letter_s_test

	
{% if is_incremental() %}

  -- this filter will only be applied on an incremental run
  where modified_timestamp >= (select max(modified_timestamp) from {{ this }})
  --where modified_timestamp > (select modified_timestamp from {{ this }})

{% endif %}
