{{ config(materialized='incremental',unique_key = 'serial_num') }}

WITH ABC AS 
(SELECT f.row_wid,
 f.serial_num,
 f.x_pr_hbsag,
 f.x_pr_hiv,
 f.x_pr_htlv,
 f.x_pr_hcv,
 f.x_pr_nat,
 f.x_pr_syp,
 f.x_pr_hbcab,
 f.x_install_dt_wid,
 f.x_test_dt_wid,
 f.x_pr_test_dt,
 f.x_pr_idm_user,
 f.x_pr_entered_dt,
 f.x_pr_result_notes,
 f.x_ct_hbsag,
 f.x_ct_hbsag_dt,
 f.x_ct_hbcore,
 f.x_ct_hbcore_dt,
 f.x_ct_hiv1,
 f.x_ct_hiv1_dt,
 f.x_ct_hiv2,
 f.x_ct_hiv2_dt,
 f.x_ct_htlv,
 f.x_ct_htlv_dt,
 f.x_ct_nat_hiv,
 f.x_ct_nat_hiv_dt,
 f.x_ct_nat_hcv,
 f.x_ct_nat_hcv_dt,
 f.x_ct_hcv,
 f.x_ct_hcv_dt,
 f.x_ct_syp,
 f.x_ct_syp_dt,
 f.x_ct_hbv,
 f.x_ct_hbv_dt,
 f.x_provider_letter_dt,
 f.x_patient_letter_dt,
 f.x_state_letter_dt,
 f.x_exception_flg,
 f.x_ct_nat_hbv,
 f.x_ct_nat_hbv_dt,
 f.x_rnt_pr_hbsag,
 f.x_rnt_pr_hiv,
 f.x_rnt_pr_htlv,
 f.x_rnt_pr_hcv,
 f.x_rnt_pr_nat,
 f.x_rnt_pr_syp,
 f.x_rnt_pr_hbcab,
 f.x_rnt_ct_hbsag,
 f.x_rnt_ct_hbcore,
 f.x_rnt_ct_hiv1,
 f.x_rnt_ct_hiv2,
 f.x_rnt_ct_htlv,
 f.x_rnt_ct_nat_hiv,
 f.x_rnt_ct_nat_hcv,
 f.x_rnt_ct_hcv,
 f.x_rnt_ct_syp,
 f.x_rnt_ct_hbv,
 f.x_rnt_ct_nat_hbv,
 f.bsl_testing_type,
 f.x_reason_not_tested,
 f.x_reason_not_tested_2,
 f.x_provider_letter_sent,
 f.x_patient_letter_snet,
 f.x_state_letter_sent,
 f.x_pr_cmv,
 f.x_pr_iatabs,
 f.x_rnt_pr_cmv,
 f.x_rnt_pr_iatabs,
 f.x_ct_hbsag_e_flg,
 f.x_ct_hbcore_e_flg,
 f.x_ct_hiv1_e_flg,
 f.x_ct_hiv2_e_flg,
 f.x_ct_htlv_e_flg,
 f.x_ct_nat_hiv_e_flg,
 f.x_ct_nat_hcv_e_flg,
 f.x_ct_hcv_e_flg,
 f.x_ct_syp_e_flg,
 f.x_ct_hbv_e_flg,
 f.x_ct_nat_hbv_e_flg,
 f.x_reconciliation_flg,
 f.x_expected_pt_flg,
 f.x_ct_hiv,
 f.x_ct_hiv_dt,
 f.x_rnt_ct_hiv,
 f.x_ct_nat,
 f.x_ct_nat_dt,
 f.x_rnt_ct_nat,
 f.x_ct_dhiv,
 f.x_ct_dhiv_dt,
 f.x_rnt_ct_dhiv,
 f.x_ct_dhiv1,
 f.x_ct_dhiv1_dt,
 f.x_rnt_ct_dhiv1,
 f.x_ct_dhbv,
 f.x_ct_dhbv_dt,
 f.x_rnt_ct_dhbv,
 f.x_ct_dhcv,
 f.x_ct_dhcv_dt,
 f.x_rnt_ct_dhcv,
 f.serial_num_sent_cts,
 f.x_pr_wnv_nat,
 f.x_rnt_pr_wnv_nat,
 f.x_ct_wnv_nat,
 f.x_ct_wnv_nat_dt,
 f.x_rnt_ct_wnv_nat,
 f.x_ct_wnv_igg,
 f.x_ct_wnv_igg_dt,
 f.x_rnt_ct_wnv_igg,
 f.x_ct_wnv_igm,
 f.x_ct_wnv_igm_dt,
 f.x_rnt_ct_wnv_igm,
 f.x_wnv_flg,
 f.modifiedtimestamp,
 
 --BIR-860 Split Letter Sent Flag and Sent Date into Separate Fields
 CASE WHEN LEFT(f.x_provider_letter_sent,1) = 'Y' THEN 'Y'
  WHEN LEFT(f.x_provider_letter_sent,1) = 'N' THEN 'N' ELSE NULL END as x_provider_letter_sent_flag,
 CASE WHEN LEFT(f.x_provider_letter_sent,1) = 'Y' AND LEN(f.x_provider_letter_sent) > 1 THEN CAST(SUBSTRING(f.x_provider_letter_sent,2,8) as date) ELSE NULL END as x_provider_letter_sent_date,
 CASE WHEN LEFT(f.x_patient_letter_snet,1) = 'Y' THEN 'Y'
  WHEN LEFT(f.x_patient_letter_snet,1) = 'N' THEN 'N' ELSE NULL END as x_patient_letter_sent_flag,
 CASE WHEN LEFT(f.x_patient_letter_snet,1) = 'Y' AND LEN(f.x_patient_letter_snet) > 1 THEN CAST(SUBSTRING(f.x_patient_letter_snet,2,8) as date) ELSE NULL END as x_patient_letter_sent_date,
 CASE WHEN LEFT(f.x_state_letter_sent,1) = 'Y' THEN 'Y'
  WHEN LEFT(f.x_state_letter_sent,1) = 'N' THEN 'N' ELSE NULL END as x_state_letter_sent_flag,
 CASE WHEN LEFT(f.x_state_letter_sent,1) = 'Y' AND LEN(f.x_state_letter_sent) > 1 THEN CAST(SUBSTRING(f.x_state_letter_sent,2,8) as date) ELSE NULL END as x_state_letter_sent_date 

FROM spectrum_cur_laboratory.idm_flow f),

DEF AS 
(SELECT serial_num,
    MAX(modifiedtimestamp) as modified_timestamp
  FROM spectrum_cur_laboratory.idm_flow
  GROUP BY serial_num),

idm_flow_s_test AS 
(SELECT abc.* FROM DEF INNER JOIN ABC
     ON abc.serial_num = def.serial_num
    AND abc.modifiedtimestamp = def.modified_timestamp) 

select * from idm_flow_s_test

{% if is_incremental() %}

  -- this filter will only be applied on an incremental run
  where modifiedtimestamp > (select modifiedtimestamp from {{ this }})

{% endif %}


