{{ config(

           materialized='incremental',
		   unique_key = 'deposit_id'
) }}

WITH ABC AS (
  SELECT ROW_NUMBER() OVER (PARTITION BY cd.deposit_id ORDER BY cd.deposit_id, cd.modified_timestamp DESC) as row_number,
  cd.id,
  cd.deposit_id,
  cd.storage_deposit_id,
  cd.kit_login_date,
  cd.blood_volume,
  cd.cell_recovery_rate,
  cd.install_date,
  cd.number_of_vials,
  cd.sample_location,
  cd.status,
  cd.total_cell_count,
  cd.cd34_count,
  cd.cd34_percent,
  cd.fc500_viability,
  cd.mbd_date,
  cd.mbd_status,
  cd.blood_volume_without_coagulant,
  cd.container_type,
  cd.send_idm_kit,
  cd.acceptable_flag,
  cd.mnc_count,
  cd.includes_nrbc,
  cd.idm_sent_flag,
  cd.number_of_containers,
  cd.processing_type,
  cd.sample_process_date,
  cd.pre_count_vtnc,
  cd.pre_count_mnc,
  cd.pre_count_percent_mnc,
  cd.pre_count_viability,
  cd.post_count_nc,
  cd.post_count_percent_mnc,
  cd.post_count_viability,
  cd.post_count_vtnc,
  cd.cells_per_ml,
  cd.blood_clots_pre,
  cd.fibrin,
  cd.sample_age_at_labeling,
  cd.percent_mnc_recovery,
  cd.pre_count_nc,
  cd.unacceptable_reason,
  cd.volume_processed,
  cd.created_by,
  cd.created_timestamp,
  cd.modified_by,
  cd.modified_timestamp,
  cd.scss_collection_device_id, 
  cd.asset_deposit_id,
  cd.mbd_deposit_id_sent_to_test,
  cd.testing_type,
  cd.flag_wnv,
  cd.collection_method,
  cd.delivery_type,
  cd.split_status,
  cd.volume_class,
  cd.anticoagulant_name,
  cd.device_type,
  cd.sample_age_ct_prep,
  cd.device_expiration_date,
  cd.device_lot_number,
  cd.device_expiration_date_mbd
FROM  spectrum_cur_laboratory.collection_device cd
),

DEF AS 
(SELECT deposit_id,
MAX(modified_timestamp) as modified_timestamp
FROM spectrum_cur_laboratory.collection_device
GROUP BY deposit_id ),

collection_device_s_test AS (
  SELECT abc.* FROM DEF INNER JOIN ABC
    ON ABC.deposit_id = DEF.deposit_id
    AND ABC.modified_timestamp = DEF.modified_timestamp )

select * from collection_device_s_test
{% if is_incremental() %}
    where modified_timestamp > (select modified_timestamp from {{ this }})
{% endif %}


