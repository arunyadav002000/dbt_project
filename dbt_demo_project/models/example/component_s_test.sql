{{ config(materialized='incremental',unique_key = 'lab_integration_id') }}

WITH ABC AS (
  SELECT c.id,
  c.deposit_id,
  c.storage_deposit_id,
  c.lab_integration_id,
  c.release_date,
  c.sample_release_id,
  c.container_type,
  c.component_status,
  c.component_type,
  c.tnc_count,
  c.cd34_count,
  c.cd34,
  c.dewar,
  c.rack,
  c.box_position,
  c.comp_row,
  c.comp_column,
  c.blood_vol_without_coagul,
  c.mnc_count,
  c.pre_count_viability,
  c.fibrin,
  c.pre_count_vtnc,
  c.blood_clots_pre,
  c.post_count_vtnc,
  c.post_count_viability,
  c.fc500_viability,
  c.created_by,
  c.create_timestamp,
  c.modified_by,
  c.modified_timestamp,
  c.scss_collection_device_id,
  c.scss_component_id
FROM  spectrum_cur_laboratory.component c
),

DEF AS 
(SELECT lab_integration_id,
MAX(modified_timestamp) as modified_timestamp
FROM spectrum_cur_laboratory.component
GROUP BY lab_integration_id),

component_s_test AS (
  SELECT abc.* FROM DEF INNER JOIN ABC
    ON ABC.lab_integration_id = DEF.lab_integration_id
    AND ABC.modified_timestamp = DEF.modified_timestamp) 

select * from component_s_test

{% if is_incremental() %}

  -- this filter will only be applied on an incremental run
  where modified_timestamp > (select modified_timestamp from {{ this }})

{% endif %}


