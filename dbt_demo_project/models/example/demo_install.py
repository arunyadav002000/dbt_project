from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.utils.dates import days_ago

with DAG(
    dag_id="dbt-intall", 
    schedule_interval=None, 
    catchup=False, 
    start_date=days_ago(1)
    ) as dag:
    cli_command = BashOperator(
        task_id="bash_command",
        bash_command="C:/User/aryadav/.dbt --version"
    )
